package controller.ui;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import controller.Main;

public class BasicPTGGame extends StateBasedGame
{
	public BasicPTGGame()
	{
		super("ptg tcg "+Main.VERSION);
	}

	@Override
	public void initStatesList(GameContainer arg0) throws SlickException {
		addState(new MainMenu());
		addState(new CardMatchGame());
	}

	
}
