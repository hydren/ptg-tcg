package controller.ui;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

public class MainMenu extends BasicGameState
{
	private StateBasedGame game; // stored for later use
	
	@Override
	public void init(GameContainer arg0, StateBasedGame game) throws SlickException {
		this.game = game;
	}

	@Override
	public void render(GameContainer arg0, StateBasedGame game, Graphics g) throws SlickException {
		g.setColor(Color.orange);
		g.drawString("Show test match", 50, 50);
		g.drawString("Exit", 50, 75);
		
	}

	@Override
	public void update(GameContainer arg0, StateBasedGame game, int arg2) 	throws SlickException {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void keyReleased(int key, char c) {
	    switch(key) {
	    case Input.KEY_1:
	        game.enterState(2, new FadeOutTransition(Color.black), new FadeInTransition(Color.black));
	        break;
	    case Input.KEY_2:
	        // TODO: Implement later
	        break;
	    case Input.KEY_3:
	        // TODO: Implement later
	        break;
	    default:
	        break;
	    }
	}

	@Override
	public int getID() {
		return 1;
	}
}
