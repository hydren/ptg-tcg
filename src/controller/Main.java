package controller;

import java.util.logging.Level;
import java.util.logging.Logger;

import logic.card.CardCatalog;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;

import controller.ui.BasicPTGGame;

public class Main {

	public static final String VERSION = "0.1.0";
	public static void main(String[] args)
	{
		CardCatalog.loadFromPropertiesFolder("test/cards", true);
		
		try
		{
			AppGameContainer appgc=null;
			appgc = new AppGameContainer(new BasicPTGGame());
			appgc.setDisplayMode(640, 480, false);
			appgc.start();
		}
		catch (SlickException ex)
		{
			Logger.getLogger(BasicPTGGame.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

}
