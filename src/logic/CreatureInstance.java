package logic;

import java.util.List;
import java.util.Set;

import logic.card.CreatureCard;
import logic.card.EnergyCard;

public class CreatureInstance 
{
	public CreatureCard currentCreatureStage;
	public List<CreatureCard> evolutionScheme;
	
	public Set<EnergyCard> attachedEnergies;
	
	public int damage;
}
