package logic;

import java.util.ArrayList;
import java.util.List;

import logic.card.Card;

public class PlayerField 
{
	public Player player;
	
	public List<Card> 
		currentDeck = new ArrayList<Card>(), 
		prizes = new ArrayList<Card>(), 
		discardPile = new ArrayList<Card>(), 
		hand = new ArrayList<Card>();

	public List<CreatureInstance> bench = new ArrayList<CreatureInstance>();
	
	public CreatureInstance activeCreature;

	public PlayerField(Player player) {
		this.player = player;
	}
}
