package logic;

public enum ColorType 
{
	//TODO make this parameters set with properties
	COLORLESS("Colorless"), WATER("Water"), FIRE("Fire"), GRASS("Grass"), LIGHTNING("Lightning"), FIGHTING("Fighting"), PSYCHIC("Psychic");

	String name;
	
	ColorType(String name)
	{
		this.name = name;
	}
	
	public String toString()
	{
		return name;
	}
	
	public static ColorType getType(String typeName) 
	{
		try
		{
			return ColorType.valueOf(typeName);
		}
		catch(Exception e)
		{
			return null;
		}
	}
}
