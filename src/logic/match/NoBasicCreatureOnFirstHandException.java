package logic.match;

import logic.PlayerField;

public class NoBasicCreatureOnFirstHandException extends Exception
{
	PlayerField player;
	public NoBasicCreatureOnFirstHandException(PlayerField player) 
	{
		this.player = player;
	}
}
