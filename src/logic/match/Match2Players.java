package logic.match;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import logic.Player;
import logic.PlayerField;
import logic.card.BasicCreatureCard;
import logic.card.Card;

public class Match2Players 
{
	public PlayerField player1, player2, currentTurnPlayer;
	public int numberOfPrizes;
	
	int currentTurn;
	void setTurn(int turn)
	{
		currentTurn = turn;
		currentTurnPlayer = turn==0?player1:player2;
		for(MatchListener listener : listeners) listener.playerTurn(currentTurnPlayer);
	}
	
	Set<MatchListener> listeners = new HashSet<MatchListener>();
	
	Random random = new Random(System.nanoTime());
	
	public Match2Players(Player p1, Player p2, int prizes)
	{
		player1 = new PlayerField(p1);
		player2 = new PlayerField(p2);
		this.numberOfPrizes = prizes;
	}
	
	public interface MatchListener
	{
		void noBasicCreatureOnFirstHand(Player p, List<Card> hand);
		void playerTurn(PlayerField p);
		void playerFirstTurn(Player p);
		void cardDraw(PlayerField p, int number);
		BasicCreatureCard selectFirstActiveCreature(List<Card> hand);
	}

	public void setupPlayerField(PlayerField player, int prizes)
	{
		player.currentDeck = new ArrayList<Card>(player.player.deck);
		Collections.shuffle(player.currentDeck, random);
		
		boolean basicCreatureFound = false;
		do 
		{
			while(player.hand.size() < 7)
			{
				Card drewCard = player.currentDeck.remove(0);
				player.hand.add(drewCard);
				if(drewCard instanceof BasicCreatureCard) basicCreatureFound = true;
			}
			if(!basicCreatureFound)
			{
				for(MatchListener listener : listeners) listener.noBasicCreatureOnFirstHand(player.player, player.hand);
				while(player.hand.size() > 0)
				{
					player.currentDeck.add(player.hand.remove(0));
				}
				Collections.shuffle(player.currentDeck, random);
			}
		}while(!basicCreatureFound);
		
		while(player.prizes.size() < prizes)
		{
			player.prizes.add(player.currentDeck.remove(0));
		}
	}
	
	public void setupMatch()
	{
		setupPlayerField(player1, numberOfPrizes);
		setupPlayerField(player2, numberOfPrizes);
	}
	
	public void start()
	{
		if(random.nextBoolean() == true)
		{
			currentTurn = 0;
			for(MatchListener listener : listeners) 
			{
				listener.playerFirstTurn(player1.player);
				listener.selectFirstActiveCreature(player1.hand);
			}
			currentTurn = 1;
			for(MatchListener listener : listeners) 
			{
				listener.playerFirstTurn(player2.player);
				listener.selectFirstActiveCreature(player2.hand);
			}
			setTurn(0);
		}
		else
		{
			currentTurn = 1;
			for(MatchListener listener : listeners) 
			{
				listener.playerFirstTurn(player2.player);
				listener.selectFirstActiveCreature(player2.hand);
			}
			currentTurn = 0;
			for(MatchListener listener : listeners) 
			{
				listener.playerFirstTurn(player1.player);
				listener.selectFirstActiveCreature(player1.hand);
			}
			setTurn(1);
		}
		
		boolean finished = false;
		while(!finished)
		{
			for(MatchListener listener : listeners) listener.cardDraw(currentTurnPlayer, 1);
			//choose actions remotely
			//TODO
		}
		
		//
	}
}
