package logic.card;

import java.awt.PageAttributes.ColorType;

import logic.effect.Effect;

public abstract class CreatureCard extends Card
{
	CreatureCard(long id) 
	{
		super(id);
	}
	String creatureName;
	int HP;
	int retreatCost;
	ColorType type, weakness, resistance;
	Effect[] effects;
}
