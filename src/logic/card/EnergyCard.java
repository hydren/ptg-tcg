package logic.card;

import java.util.ArrayList;
import java.util.Properties;

import logic.ColorType;

public class EnergyCard extends Card
{
	private static final long serialVersionUID = 6708524215835728435L;

	EnergyCard(long id) 
	{
		super(id);
	}
	
	EnergyCard(long id, Properties p)
	{
		super(id);
		name = p.getProperty("name");
		
		String value;
		
		ArrayList<ColorType> typesFound = new ArrayList<ColorType>();
		value = p.getProperty("providedEnergy");
		
		if(value == null)
		{
			this.providedTypes = new ColorType[]{ColorType.COLORLESS};
			System.out.println("Warning: No information about provided energy on card "+name+". Assuming colorless to avoid errors.");
		}
		else for(String type_str : value.split(","))
		{
			if(type_str.isEmpty())
			{
				System.out.println("Warning: Empty value specified on provided energy on card "+name+". Ignoring.");
				continue;
			}
			if(ColorType.getType(type_str) == null)
			{
				System.out.println("Warning: Unrecognized energy \""+type_str+"\" provided by card \""+name+"\". Assuming colorless to avoid errors.");
				typesFound.add(ColorType.COLORLESS);
			}
			else typesFound.add(ColorType.getType(type_str));
		}
		this.providedTypes = typesFound.toArray(new ColorType[0]);
	}

	public ColorType[] providedTypes;

}
