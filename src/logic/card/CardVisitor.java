package logic.card;

public interface CardVisitor
{
	void onCard(Card card);
	void onCard(EnergyCard card);
	//...
}

