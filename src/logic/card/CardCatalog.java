package logic.card;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Properties;

public class CardCatalog extends HashSet<Card>
{
	private static final long serialVersionUID = -7833536888691721605L;
	
	private static final CardCatalog instance = new CardCatalog();
	
	public static final CardCatalog getInstance()
	{
		return instance;
	}
	
	/** Load all card properties files from the specified folder. If recursive is true, it will also search for files in subfolders. */
	public static void loadFromPropertiesFolder(String foldername, boolean recursive)
	{
		File folder = new File(foldername);
		if(folder.isDirectory() == false) return;
		lookForFilesInFolder(folder, recursive);
	}
	
	private static void lookForFilesInFolder(File parentFolder, boolean recursive)
	{
		for(File f : parentFolder.listFiles())
		{
			if(f.isFile() && f.getName().endsWith(".card"))
			{
				loadPropertyFile(f);
			}
			else if(f.isDirectory() && recursive==true)
			{
				lookForFilesInFolder(f, true);
			}
		}
	}
	
	private static void loadPropertyFile(File file)
	{
		if(file == null || file.isFile() == false) return;
		
		FileInputStream fis = null;
		Properties properties = new Properties();
		try 
		{
			fis = new FileInputStream(file);
			properties.load(fis);
		} 
		catch (FileNotFoundException e) { e.printStackTrace(); } 
		catch (IOException e) 
		{
			System.out.println("Error loading file "+file.getAbsolutePath()+". Ignoring...");
			return;
		}
		
		try
		{
			Card card = loadCardFromProperties(properties);
			CardCatalog.getInstance().add(card);
		}
		catch (Exception e) 
		{
			System.out.println("Ill-formatted card property file "+file.getAbsolutePath()+". "+e.getMessage()+" Ignoring...");
		}
		
		//clean up resources
		try 
		{
			fis.close();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	//Gather all card info from properties file
	private static Card loadCardFromProperties(Properties p) throws Exception, NumberFormatException
	{
		//FIXME finish to implement this
		Card card = null;
		String value;
		
		value = p.getProperty("id");
		if(value == null)
			throw new Exception("Invalid property format: no class defined!");
		
		long id = Long.parseLong(p.getProperty("id"));
		
		value = p.getProperty("class");
		if(value == null)
			throw new Exception("Invalid property format: no class defined!");
		
		if(value.equals(EnergyCard.class.getSimpleName()))
			card = new EnergyCard(id, p);
		
		else if(value.equals(BasicCreatureCard.class.getSimpleName()))
			card = new BasicCreatureCard(id, p);
		
		//... TODO more card types here
		
		else throw new Exception("Unrecognized card class: "+value);
		
		return card;
	}
}
