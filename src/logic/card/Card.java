package logic.card;

import java.io.Serializable;
import java.lang.reflect.Method;

public class Card implements Serializable
{
	
	private static final long serialVersionUID = -7973457129202869077L;
	public String name;
	public final long id;
	
	Card(long id)
	{
		this.id = id;
	}
	
	//auxiliary dispatch method
	public final void accept(CardVisitor x)
	{
		for(Method method : CardVisitor.class.getMethods())
		{
			if(method.getName().equals("onCard") && method.getParameterTypes()[0].equals(this.getClass()))
			{
				try { method.invoke(x, this); } 
				catch (Exception e) { System.out.println("Could not call onCard() for class "+this.getClass().getName()); }
				break;
			}
		}
	}
}
