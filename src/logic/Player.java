package logic;

import java.util.Set;

import logic.card.Card;

public class Player
{
	public String name;
	public Set<Card> deck;
}
