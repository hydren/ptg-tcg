package rendering;

import org.newdawn.slick.Graphics;

import logic.card.Card;
import logic.card.CardVisitor;
import logic.card.EnergyCard;

public class CardRenderer implements CardVisitor
{
	private Graphics graphics;

	public Graphics getGraphics() {
		return graphics;
	}

	public CardRenderer setGraphics(Graphics graphics) {
		this.graphics = graphics;
		return this;
	}
	
	@Override
	public void onCard(Card card) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCard(EnergyCard card) {
		// TODO Auto-generated method stub
		
	}
}
